/*
** client.h for client in /home/doghri_f/rendu/PSU_2013_minitalk/Client
** 
** Made by doghri_f
** Login   <doghri_f@epitech.net>
** 
** Started on  Sat Mar 22 16:54:03 2014 doghri_f
** Last update Sat Mar 22 17:01:37 2014 doghri_f
*/

#ifndef CLIENT_H_
# define CLIENT_H_

int calc(char *str, int b);
int my_get_nbr(char *str);

void my_putchar(char c);
int my_putstr(char *str);

int     char_bin(int pid, int z, char **av, int i);
int     main(int ac, char **av);

#endif /* !CLEINT_H_ */
