/*
** my_putstr.c for my_putstr in /home/doghri_f/rendu/PSU_2013_minitalk
** 
** Made by doghri_f
** Login   <doghri_f@epitech.net>
** 
** Started on  Sun Mar 16 12:57:42 2014 doghri_f
** Last update Sun Mar 16 16:45:10 2014 doghri_f
*/

void	my_putchar(char c)
{
  write(1, &c, 1);
}

int	my_putstr(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    my_putchar(str[i++]);
}
