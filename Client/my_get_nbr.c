/*
** my_get_nbr.c for my_get_nbr in /home/doghri_f/rendu/Allum1
** 
** Made by doghri_f
** Login   <doghri_f@epitech.net>
** 
** Started on  Tue Feb  4 13:05:29 2014 doghri_f
** Last update Fri Feb 14 12:18:19 2014 doghri_f
*/

int	calc(char *str, int b)
{
  int	i;
  int	y;

  y = 0;
  i = 0;
  while (b >= 1)
    {
      if (str[y] >= '0' && str[y] <= '9')
	{
	  i = (str[y] - 48) * b + i;
	  y++;
	  b = b / 10;
	}
      else
	y++;
    }
  return (i);
}

int	my_get_nbr(char *str)
{
  int	y;
  long	b;
  int	nbr;

  y = 0;
  b = 1;
  while (str[y] >= '0' && str[y] <= '9' || str[y] == '-')
    {
      if (str[y] >= '0' && str[y] <= '9')
	{
	  b = b * 10;
	  y++;
	}
      else
	y++;
    }
  b = b / 10;
  nbr = calc(str, b);
  if (str[0] == '-')
    nbr = nbr * (-1);
  return (nbr);
}
