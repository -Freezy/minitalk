/*
** main.c for main in /home/doghri_f/rendu/PSU_2013_minitalk/client
** 
** Made by doghri_f
** Login   <doghri_f@epitech.net>
** 
** Started on  Sun Mar 16 16:44:17 2014 doghri_f
** Last update Sat Mar 22 17:03:33 2014 doghri_f
*/

#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include "client.h"

int	char_bin(int pid, int z, char **av, int i)
{
  int	bin;

  bin = 0;
  while (z < 8)
    {
      bin = (av[2][i] >> z++) & 1;
      if (bin == 1)
	kill(pid, SIGUSR1);
      if (bin == 0)
	kill(pid, SIGUSR2);
      usleep(500);
    }
}

int	main(int ac, char **av)
{
  int	i;
  int	pid;
  int	z;

  i = 0;
  if (ac <= 2)
    {
      my_putstr("[PID] [MSG]\n");
      return (0);
    }
  if ((pid = my_get_nbr(av[1])) < 1)
    {
      my_putstr("NOPE you're not going to f*** me ! again... !\n");
      return (0);
    }
  while (av[2][i])
    {
      z = 0;
      char_bin(pid, z, av, i);
      i++;
    }
}
