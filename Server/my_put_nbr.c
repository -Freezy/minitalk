/*
** my_put_nbr.c for my put nbr in /home/doghri_f/rendu/Piscine-C-lib/my
** 
** Made by doghri_f
** Login   <doghri_f@epitech.net>
** 
** Started on  Wed Oct  9 09:31:15 2013 doghri_f
** Last update Mon Feb 10 10:48:53 2014 doghri_f
*/

int	my_put_nbr(int nb)
{
  int	a;
  long	b;

  b = 1;
  if (nb == 0)
    my_putchar(48);
  if (nb < 0)
    {
      my_putchar('-');
      nb = nb * - b;
    }
  while (nb != nb % b)
    b = b * 10;
  b = b / 10;
  while (b != 0)
    {
      a = nb / b;
      my_putchar(a + 48);
      nb = nb % b;
      b = b / 10;
    }
}
