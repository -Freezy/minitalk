/*
** server.c for my_server in /home/doghri_f/rendu/PSU_2013_minitalk
** 
** Made by doghri_f
** Login   <doghri_f@epitech.net>
** 
** Started on  Tue Mar 18 12:23:38 2014 doghri_f
** Last update Sat Mar 22 17:03:17 2014 doghri_f
*/

#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include "server.h"

int	g_sig;

void	catch_sig(int i)
{
  static int	z = 0;

  if (i == SIGUSR1)
    g_sig = (1 << z) | g_sig;
  else if (i == SIGUSR2)
    g_sig = (0 << z) | g_sig;
  z++;
  if (z == 8)
    {
      z = 0;
      my_putchar(g_sig);
      g_sig = 0;
    }
}

int	main()
{
  int	pid;
  int	i;

  pid = getpid();
  my_putstr("PID = ");
  my_put_nbr(pid);
  my_putchar('\n');
  i = 0;
  g_sig = 0;
  while (1)
    {
      signal(SIGUSR1, catch_sig);
      signal(SIGUSR2, catch_sig);
      sleep(10);
    }
}
