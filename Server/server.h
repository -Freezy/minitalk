/*
** server.h for server in /home/doghri_f/rendu/PSU_2013_minitalk/Server
** 
** Made by doghri_f
** Login   <doghri_f@epitech.net>
** 
** Started on  Sat Mar 22 16:59:36 2014 doghri_f
** Last update Sat Mar 22 17:03:03 2014 doghri_f
*/

#ifndef SERVER_H_
# define SERVER_H_

int     my_put_nbr(int nb);

void my_putchar(char c);
int my_putstr(char *str);

void catch_sig(int i);
int main();

#endif /* !SERVER_H_ */
