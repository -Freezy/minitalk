##
## Makefile for Makefile in /home/doghri_f/rendu/PSU_2013_minishell1
## 
## Made by doghri_f
## Login   <doghri_f@epitech.net>
## 
## Started on  Thu Jan  2 16:57:52 2014 doghri_f
## Last update Sat Mar 22 17:04:17 2014 doghri_f
##

CC	= 	cc

RM	= 	rm -f

SRCS	= 	Client/my_putstr.c \
		Client/client.c \
		Client/my_get_nbr.c

SRCS2	=	Server/server.c \
		Server/my_put_nbr.c \
		Server/my_putstr.c

OBJS	= 	$(SRCS:.c=.o)

OBJS2	=	$(SRCS2:.c=.o)

NAME 	= 	Client/client

NAME2	=	Server/server

all:		$(NAME) $(NAME2)

$(NAME): 	$(OBJS)
		$(CC) $(OBJS) -o $(NAME)

$(NAME2):	$(OBJS2)
		$(CC) $(OBJS2) -o $(NAME2)

clean:
		$(RM) $(OBJS)
		$(RM) $(OBJS2)

fclean:		clean
		$(RM) $(NAME)
		$(RM) $(NAME2)

re: 		fclean all

.PHONY: 	all clean fclean re
